# Copyright (C) The IETF Trust (2014)
#

DOCNAME=draft-ietf-nfsv4-scsi-layout-nvme-07

FILE=scsi-nvme
PREVVERS=06
VERS=05

YEAR=`date +%Y`
MONTH=`date +%m`
DAY=`date +%d`

KRAMDOWNRFC=kramdown-rfc
XML2RFC=xml2rfc

all: $(DOCNAME).xml $(DOCNAME).txt $(DOCNAME).html


autogen.md: $(FILE).md
	sed \
		-e s/DOCNAMEVAR/${DOCNAME}/g \
		-e s/DAYVAR/${DAY}/g \
		-e s/MONTHVAR/${MONTH}/g \
		-e s/YEARVAR/${YEAR}/g \
		< $(FILE).md > $@

$(DOCNAME).xml: autogen.md
	$(KRAMDOWNRFC) -3 autogen.md > $@

$(DOCNAME).txt: $(DOCNAME).xml
	$(XML2RFC) --text $(DOCNAME).xml -o $@

$(DOCNAME).html: $(DOCNAME).xml
	$(XML2RFC) --html $(DOCNAME).xml -o $@


clean:
	rm -f autogen.md
